# STM32F103C8T6 最小系统电路 PCB 绘制图资源

## 简介
本仓库提供了一个使用嘉立创 EDA（标准版）和 Altium Designer（AD）绘制的 STM32F103C8T6 最小系统电路 PCB 资源文件。该最小系统电路包括微控制器、电源电路、时钟电路、复位电路以及程序下载接口等关键组件。

## 资源内容
- **嘉立创 EDA（标准版）文件**：包含使用嘉立创 EDA 绘制的 STM32F103C8T6 最小系统电路 PCB 设计文件。
- **Altium Designer（AD）文件**：包含使用 Altium Designer 绘制的 STM32F103C8T6 最小系统电路 PCB 设计文件。

## 使用说明
1. **嘉立创 EDA（标准版）**：
   - 下载并安装嘉立创 EDA（标准版）软件。
   - 打开本仓库中的嘉立创 EDA 文件，即可查看和编辑 PCB 设计。

2. **Altium Designer（AD）**：
   - 下载并安装 Altium Designer 软件。
   - 打开本仓库中的 Altium Designer 文件，即可查看和编辑 PCB 设计。

## 注意事项
- 本资源文件仅供学习和参考使用，请勿用于商业用途。
- 如有任何问题或建议，欢迎在仓库中提出 Issue 或联系博主。

## 获取方式
积分不够的朋友，可以点波关注，博主将无偿提供资源！

## 联系博主
- 邮箱：example@example.com
- 微信公众号：example

感谢您的关注与支持！